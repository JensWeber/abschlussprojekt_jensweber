#!/usr/bin/python

import sys

from PySide2.QtWidgets import QApplication, QPushButton, QLabel, QListView, QGridLayout, QDialog, QWidget, QMainWindow
from PySide2.QtCore import Slot, QFile
from PySide2 import QtCore, QtGui
from PySide2.QtCore import *
import random



@Slot()
def say_hello():
 print("Button clicked, Hello!")


class Form(QDialog):

 def __init__(self, parent=None):
  super(Form, self).__init__(parent)
  self.setWindowTitle("Visualisierung")
  self.layout = QGridLayout()

  self.hello = ["Hallo Welt", "你好，世界", "Hei maailma",
                "Hola Mundo", "Привет мир"]
  self.button = QPushButton('Test')
  self.text = QLabel("Hello World")
  self.layout.addWidget(self.button)
  self.layout.addWidget(self.text, 3, 1)

  self.button.clicked.connect(self.magic)


  self.createTraceListView()

  self.createTableListView()

 def magic(self):
     self.text.setText(random.choice(self.hello))

 def createTabs(self):
     tab1 = QWidget()
     tab2 = QWidget()
     tab3 = QWidget()
     tab4 = QWidget()

     self.layout.addWidget(tab1, 1, 1)
     self.layout.addWidget(tab2, 1, 2)
     self.layout.addWidget(tab3, 1, 3)
     self.layout.addWidget(tab4, 1, 4)

     tab1.tabUI

 def createTableListView(self):

      listView = QListView()
      self.layout.addWidget(listView, 2, 0)
      self.setLayout(self.layout)

      entries = ['Vollautomatik             X',
                 'Hand',
                 'Fährt vorwärts',
                 'Fährt Rückwärts',
                 'Teilautomatik',
                 'Betriebsbereit',
                 'Muli 1 in Pos.',
                 'Muli 2 in Pos.']

      model = QtGui.QStandardItemModel()
      listView.setModel(model)

      for i in entries:
       item = QtGui.QStandardItem(i)
       model.appendRow(item)

      listView.show()

 def createTraceListView(self):

  listView = QListView()
  self.layout.addWidget(listView, 1, 0)
  self.setLayout(self.layout)

  entries = ['13.01 01:08:46:369 @06 !0! CShuttle::Status - TW=2 FKT=DL0000 SL0000 DR0000 SR0000 TT0000',
             '13.01 01:08:46:369 @06 !0! CShuttle::Status - TW=2, A-Tracks: 1L=0 2L=236 5L=0 1R=0 2R=0 5R=0',
             '13.01 01:08:47:323 @06 !0! CShuttle::Status - TW=2, A-Tracks: 1L=0 2L=0 5L=0 1R=0 2R=0 5R=0',
             '13.01 01:08:55:968 @06 !0! CShuttle::Status - TW=2 FKT=DL0000 SL0000 DR0000 SR0000 TT0000']

  model = QtGui.QStandardItemModel()
  listView.setModel(model)

  for i in entries:
   item = QtGui.QStandardItem(i)
   model.appendRow(item)

  listView.show()


if __name__ == '__main__':

    app = QApplication(sys.argv)

    form = Form()
    form.show()



    sys.exit(app.exec_())
