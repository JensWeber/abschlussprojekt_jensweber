import sys
import re
import binascii

from PySide2.QtCore import (QCoreApplication, QMetaObject, QObject, QPoint,
    QRect, QSize, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QLinearGradient, QPalette, QPainter, QPixmap,
    QRadialGradient)
from PySide2.QtWidgets import *
from PySide2 import QtWidgets
from PySide2.QtWidgets import QFileDialog
from PySide2.QtCore import QDir
from PySide2.QtCore import *
from PySide2.QtGui import QStandardItemModel,QStandardItem
from PySide2.QtWidgets import QMessageBox

FUNCTION_S_VALUE_NAMES = [
    'bPut1L',
    'bGet1L',
    'bPut1R',
    'bGet1R',

    'bPut2L',
    'bGet2L',
    'bPut2R',
    'bGet2R',

    'bPut3L',
    'bGet3L',
    'bPut3R',
    'bGet3R',

    'bPut4L',
    'bGet4L',
    'bPut4R',
    'bGet4R'
]

STAT_VALUE_NAMES = [
    'bAuto',
    'bMan',
    'bForward',
    'bBackward',
    'bManAuto',
    'bReady',
    'bMuli1OnTw'
    'bMuli2OnTw',

    'bEmptyM1',
    'bEmptyM2',
    'bEmptyM3',
    'bEmptyM4',
    'bEmptyM12',
    'bEmptyM23',
    'bEmptyM34',
    'bReserved2',

    'bEmpty1',
    'bEmpty2',
    'bEmpty3',
    'bEmpty4',
    'bEmpty12',
    'bEmpty23',
    'bEmpty34',
    'bReserved3',

    'bEmptyR1',
    'bEmptyR2',
    'bEmptyR3',
    'bEmptyR4',
    'bEmptyR12',
    'bEmptyR23',
    'bEmptyR34',
    'bReserved4',
]


def is_bit_set(n, k):
    return n >> k & 1

def parse_hex_value(value_hex_str, value_names, num_bits=16):
    value_int = int(value_hex_str, 16)
    values = {value_names[i]:is_bit_set(value_int, i) for i in range(0, num_bits-1)}
    return values

def parse_status_log_line(log_line):
    pattern = r'TW(?P<shuttle_id>\d+) FKT=DL(?P<function_d>[A-Z0-9]{4}) SL(?P<function_s>[A-Z0-9]{4}) DR([A-Z0-9]{4}) SR([A-Z0-9]{4}) TT([A-Z0-9]{4}) STAT=(?P<stat>[A-Z0-9]{8})'
    matches = re.search(pattern, log_line)

    if not matches:
        return None

    return {
        'stat': parse_hex_value(matches.group('stat'), STAT_VALUE_NAMES, num_bits=32),
        'function_s': parse_hex_value(matches.group('function_s'), FUNCTION_S_VALUE_NAMES)
    }

def parse_order_log_line(log_line):
    pattern = r''
    matches = re.search(pattern, log_line)

    if not matches:
        return None

    return {
        'stat': parse_hex_value(matches.group('stat'), STAT_VALUE_NAMES, num_bits=32),
        'function_s': parse_hex_value(matches.group('function_s'), FUNCTION_S_VALUE_NAMES)
    }



class Ui_Visualisierung(object):

    def setupUi(self, Visualisierung):
        if Visualisierung.objectName():
            Visualisierung.setObjectName(u"Visualisierung")
        Visualisierung.resize(977, 860)
        self.importAction = QAction(Visualisierung)
        self.importAction.setObjectName(u"actionOpen")
        self.centralwidget = QWidget(Visualisierung)
        self.centralwidget.setObjectName(u"centralwidget")
        self.gridLayout = QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.listView = QListView(self.centralwidget)
        self.listView.setObjectName(u"listView")

        self.verticalLayout_2.addWidget(self.listView)

        self.Status = QTabWidget(self.centralwidget)
        self.Status.setObjectName(u"Status")
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.tableWidget = QTableWidget(self.tab)
        if (self.tableWidget.columnCount() < 1):
            self.tableWidget.setColumnCount(1)
        __qtablewidgetitem = QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, __qtablewidgetitem)
        if (self.tableWidget.rowCount() < 8):
            self.tableWidget.setRowCount(8)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(0, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(1, __qtablewidgetitem2)
        __qtablewidgetitem3 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(2, __qtablewidgetitem3)
        __qtablewidgetitem4 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(3, __qtablewidgetitem4)
        __qtablewidgetitem5 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(4, __qtablewidgetitem5)
        __qtablewidgetitem6 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(5, __qtablewidgetitem6)
        __qtablewidgetitem7 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(6, __qtablewidgetitem7)
        __qtablewidgetitem8 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(7, __qtablewidgetitem8)
        __qtablewidgetitem9 = QTableWidgetItem()
        self.tableWidget.setItem(0, 0, __qtablewidgetitem9)
        self.tableWidget.setObjectName(u"tableWidget")
        self.tableWidget.setGeometry(QRect(210, 40, 141, 265))
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tableWidget.sizePolicy().hasHeightForWidth())
        self.tableWidget.setSizePolicy(sizePolicy)
        self.tableWidget.setMaximumSize(QSize(741, 16777215))
        self.tableWidget_2 = QTableWidget(self.tab)
        if (self.tableWidget_2.columnCount() < 1):
            self.tableWidget_2.setColumnCount(1)
        __qtablewidgetitem10 = QTableWidgetItem()
        self.tableWidget_2.setHorizontalHeaderItem(0, __qtablewidgetitem10)
        if (self.tableWidget_2.rowCount() < 8):
            self.tableWidget_2.setRowCount(8)
        __qtablewidgetitem11 = QTableWidgetItem()
        self.tableWidget_2.setVerticalHeaderItem(0, __qtablewidgetitem11)
        __qtablewidgetitem12 = QTableWidgetItem()
        self.tableWidget_2.setVerticalHeaderItem(1, __qtablewidgetitem12)
        __qtablewidgetitem13 = QTableWidgetItem()
        self.tableWidget_2.setVerticalHeaderItem(2, __qtablewidgetitem13)
        __qtablewidgetitem14 = QTableWidgetItem()
        self.tableWidget_2.setVerticalHeaderItem(3, __qtablewidgetitem14)
        __qtablewidgetitem15 = QTableWidgetItem()
        self.tableWidget_2.setVerticalHeaderItem(4, __qtablewidgetitem15)
        __qtablewidgetitem16 = QTableWidgetItem()
        self.tableWidget_2.setVerticalHeaderItem(5, __qtablewidgetitem16)
        __qtablewidgetitem17 = QTableWidgetItem()
        self.tableWidget_2.setVerticalHeaderItem(6, __qtablewidgetitem17)
        __qtablewidgetitem18 = QTableWidgetItem()
        self.tableWidget_2.setVerticalHeaderItem(7, __qtablewidgetitem18)
        __qtablewidgetitem19 = QTableWidgetItem()
        self.tableWidget_2.setItem(0, 0, __qtablewidgetitem19)
        __qtablewidgetitem20 = QTableWidgetItem()
        self.tableWidget_2.setItem(3, 0, __qtablewidgetitem20)
        self.tableWidget_2.setObjectName(u"tableWidget_2")
        self.tableWidget_2.setGeometry(QRect(350, 40, 121, 265))
        self.tableWidget_2.setMaximumSize(QSize(741, 16777215))
        self.tableWidget_4 = QTableWidget(self.tab)
        if (self.tableWidget_4.columnCount() < 1):
            self.tableWidget_4.setColumnCount(1)
        __qtablewidgetitem21 = QTableWidgetItem()
        self.tableWidget_4.setHorizontalHeaderItem(0, __qtablewidgetitem21)
        if (self.tableWidget_4.rowCount() < 8):
            self.tableWidget_4.setRowCount(8)
        __qtablewidgetitem22 = QTableWidgetItem()
        self.tableWidget_4.setVerticalHeaderItem(0, __qtablewidgetitem22)
        __qtablewidgetitem23 = QTableWidgetItem()
        self.tableWidget_4.setVerticalHeaderItem(1, __qtablewidgetitem23)
        __qtablewidgetitem24 = QTableWidgetItem()
        self.tableWidget_4.setVerticalHeaderItem(2, __qtablewidgetitem24)
        __qtablewidgetitem25 = QTableWidgetItem()
        self.tableWidget_4.setVerticalHeaderItem(3, __qtablewidgetitem25)
        __qtablewidgetitem26 = QTableWidgetItem()
        self.tableWidget_4.setVerticalHeaderItem(4, __qtablewidgetitem26)
        __qtablewidgetitem27 = QTableWidgetItem()
        self.tableWidget_4.setVerticalHeaderItem(5, __qtablewidgetitem27)
        __qtablewidgetitem28 = QTableWidgetItem()
        self.tableWidget_4.setVerticalHeaderItem(6, __qtablewidgetitem28)
        __qtablewidgetitem29 = QTableWidgetItem()
        self.tableWidget_4.setVerticalHeaderItem(7, __qtablewidgetitem29)
        __qtablewidgetitem30 = QTableWidgetItem()
        self.tableWidget_4.setItem(0, 0, __qtablewidgetitem30)
        self.tableWidget_4.setObjectName(u"tableWidget_4")
        self.tableWidget_4.setGeometry(QRect(470, 40, 121, 265))
        self.tableWidget_4.setMaximumSize(QSize(741, 16777215))
        self.tableWidget_3 = QTableWidget(self.tab)
        if (self.tableWidget_3.columnCount() < 1):
            self.tableWidget_3.setColumnCount(1)
        __qtablewidgetitem31 = QTableWidgetItem()
        self.tableWidget_3.setHorizontalHeaderItem(0, __qtablewidgetitem31)
        if (self.tableWidget_3.rowCount() < 8):
            self.tableWidget_3.setRowCount(8)
        __qtablewidgetitem32 = QTableWidgetItem()
        self.tableWidget_3.setVerticalHeaderItem(0, __qtablewidgetitem32)
        __qtablewidgetitem33 = QTableWidgetItem()
        self.tableWidget_3.setVerticalHeaderItem(1, __qtablewidgetitem33)
        __qtablewidgetitem34 = QTableWidgetItem()
        self.tableWidget_3.setVerticalHeaderItem(2, __qtablewidgetitem34)
        __qtablewidgetitem35 = QTableWidgetItem()
        self.tableWidget_3.setVerticalHeaderItem(3, __qtablewidgetitem35)
        __qtablewidgetitem36 = QTableWidgetItem()
        self.tableWidget_3.setVerticalHeaderItem(4, __qtablewidgetitem36)
        __qtablewidgetitem37 = QTableWidgetItem()
        self.tableWidget_3.setVerticalHeaderItem(5, __qtablewidgetitem37)
        __qtablewidgetitem38 = QTableWidgetItem()
        self.tableWidget_3.setVerticalHeaderItem(6, __qtablewidgetitem38)
        __qtablewidgetitem39 = QTableWidgetItem()
        self.tableWidget_3.setVerticalHeaderItem(7, __qtablewidgetitem39)
        __qtablewidgetitem40 = QTableWidgetItem()
        self.tableWidget_3.setItem(0, 0, __qtablewidgetitem40)
        __qtablewidgetitem41 = QTableWidgetItem()
        self.tableWidget_3.setItem(2, 0, __qtablewidgetitem41)
        self.tableWidget_3.setObjectName(u"tableWidget_3")
        self.tableWidget_3.setGeometry(QRect(590, 40, 121, 265))
        self.tableWidget_3.setMaximumSize(QSize(741, 16777215))
        self.Status.addTab(self.tab, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.tableWidget_13 = QTableWidget(self.tab_2)
        if (self.tableWidget_13.columnCount() < 1):
            self.tableWidget_13.setColumnCount(1)
        __qtablewidgetitem42 = QTableWidgetItem()
        self.tableWidget_13.setHorizontalHeaderItem(0, __qtablewidgetitem42)
        if (self.tableWidget_13.rowCount() < 8):
            self.tableWidget_13.setRowCount(8)
        __qtablewidgetitem43 = QTableWidgetItem()
        self.tableWidget_13.setVerticalHeaderItem(0, __qtablewidgetitem43)
        __qtablewidgetitem44 = QTableWidgetItem()
        self.tableWidget_13.setVerticalHeaderItem(1, __qtablewidgetitem44)
        __qtablewidgetitem45 = QTableWidgetItem()
        self.tableWidget_13.setVerticalHeaderItem(2, __qtablewidgetitem45)
        __qtablewidgetitem46 = QTableWidgetItem()
        self.tableWidget_13.setVerticalHeaderItem(3, __qtablewidgetitem46)
        __qtablewidgetitem47 = QTableWidgetItem()
        self.tableWidget_13.setVerticalHeaderItem(4, __qtablewidgetitem47)
        __qtablewidgetitem48 = QTableWidgetItem()
        self.tableWidget_13.setVerticalHeaderItem(5, __qtablewidgetitem48)
        __qtablewidgetitem49 = QTableWidgetItem()
        self.tableWidget_13.setVerticalHeaderItem(6, __qtablewidgetitem49)
        __qtablewidgetitem50 = QTableWidgetItem()
        self.tableWidget_13.setVerticalHeaderItem(7, __qtablewidgetitem50)
        __qtablewidgetitem51 = QTableWidgetItem()
        self.tableWidget_13.setItem(0, 0, __qtablewidgetitem51)
        __qtablewidgetitem52 = QTableWidgetItem()
        self.tableWidget_13.setItem(2, 0, __qtablewidgetitem52)
        self.tableWidget_13.setObjectName(u"tableWidget_13")
        self.tableWidget_13.setGeometry(QRect(600, 30, 121, 265))
        self.tableWidget_13.setMaximumSize(QSize(741, 16777215))
        self.tableWidget_14 = QTableWidget(self.tab_2)
        if (self.tableWidget_14.columnCount() < 1):
            self.tableWidget_14.setColumnCount(1)
        __qtablewidgetitem53 = QTableWidgetItem()
        self.tableWidget_14.setHorizontalHeaderItem(0, __qtablewidgetitem53)
        if (self.tableWidget_14.rowCount() < 8):
            self.tableWidget_14.setRowCount(8)
        __qtablewidgetitem54 = QTableWidgetItem()
        self.tableWidget_14.setVerticalHeaderItem(0, __qtablewidgetitem54)
        __qtablewidgetitem55 = QTableWidgetItem()
        self.tableWidget_14.setVerticalHeaderItem(1, __qtablewidgetitem55)
        __qtablewidgetitem56 = QTableWidgetItem()
        self.tableWidget_14.setVerticalHeaderItem(2, __qtablewidgetitem56)
        __qtablewidgetitem57 = QTableWidgetItem()
        self.tableWidget_14.setVerticalHeaderItem(3, __qtablewidgetitem57)
        __qtablewidgetitem58 = QTableWidgetItem()
        self.tableWidget_14.setVerticalHeaderItem(4, __qtablewidgetitem58)
        __qtablewidgetitem59 = QTableWidgetItem()
        self.tableWidget_14.setVerticalHeaderItem(5, __qtablewidgetitem59)
        __qtablewidgetitem60 = QTableWidgetItem()
        self.tableWidget_14.setVerticalHeaderItem(6, __qtablewidgetitem60)
        __qtablewidgetitem61 = QTableWidgetItem()
        self.tableWidget_14.setVerticalHeaderItem(7, __qtablewidgetitem61)
        __qtablewidgetitem62 = QTableWidgetItem()
        self.tableWidget_14.setItem(0, 0, __qtablewidgetitem62)
        self.tableWidget_14.setObjectName(u"tableWidget_14")
        self.tableWidget_14.setGeometry(QRect(190, 30, 141, 265))
        sizePolicy.setHeightForWidth(self.tableWidget_14.sizePolicy().hasHeightForWidth())
        self.tableWidget_14.setSizePolicy(sizePolicy)
        self.tableWidget_14.setMaximumSize(QSize(741, 16777215))
        self.tableWidget_15 = QTableWidget(self.tab_2)
        if (self.tableWidget_15.columnCount() < 1):
            self.tableWidget_15.setColumnCount(1)
        __qtablewidgetitem63 = QTableWidgetItem()
        self.tableWidget_15.setHorizontalHeaderItem(0, __qtablewidgetitem63)
        if (self.tableWidget_15.rowCount() < 8):
            self.tableWidget_15.setRowCount(8)
        __qtablewidgetitem64 = QTableWidgetItem()
        self.tableWidget_15.setVerticalHeaderItem(0, __qtablewidgetitem64)
        __qtablewidgetitem65 = QTableWidgetItem()
        self.tableWidget_15.setVerticalHeaderItem(1, __qtablewidgetitem65)
        __qtablewidgetitem66 = QTableWidgetItem()
        self.tableWidget_15.setVerticalHeaderItem(2, __qtablewidgetitem66)
        __qtablewidgetitem67 = QTableWidgetItem()
        self.tableWidget_15.setVerticalHeaderItem(3, __qtablewidgetitem67)
        __qtablewidgetitem68 = QTableWidgetItem()
        self.tableWidget_15.setVerticalHeaderItem(4, __qtablewidgetitem68)
        __qtablewidgetitem69 = QTableWidgetItem()
        self.tableWidget_15.setVerticalHeaderItem(5, __qtablewidgetitem69)
        __qtablewidgetitem70 = QTableWidgetItem()
        self.tableWidget_15.setVerticalHeaderItem(6, __qtablewidgetitem70)
        __qtablewidgetitem71 = QTableWidgetItem()
        self.tableWidget_15.setVerticalHeaderItem(7, __qtablewidgetitem71)
        __qtablewidgetitem72 = QTableWidgetItem()
        self.tableWidget_15.setItem(0, 0, __qtablewidgetitem72)
        self.tableWidget_15.setObjectName(u"tableWidget_15")
        self.tableWidget_15.setGeometry(QRect(470, 30, 121, 265))
        self.tableWidget_15.setMaximumSize(QSize(741, 16777215))
        self.tableWidget_16 = QTableWidget(self.tab_2)
        if (self.tableWidget_16.columnCount() < 1):
            self.tableWidget_16.setColumnCount(1)
        __qtablewidgetitem73 = QTableWidgetItem()
        self.tableWidget_16.setHorizontalHeaderItem(0, __qtablewidgetitem73)
        if (self.tableWidget_16.rowCount() < 8):
            self.tableWidget_16.setRowCount(8)
        __qtablewidgetitem74 = QTableWidgetItem()
        self.tableWidget_16.setVerticalHeaderItem(0, __qtablewidgetitem74)
        __qtablewidgetitem75 = QTableWidgetItem()
        self.tableWidget_16.setVerticalHeaderItem(1, __qtablewidgetitem75)
        __qtablewidgetitem76 = QTableWidgetItem()
        self.tableWidget_16.setVerticalHeaderItem(2, __qtablewidgetitem76)
        __qtablewidgetitem77 = QTableWidgetItem()
        self.tableWidget_16.setVerticalHeaderItem(3, __qtablewidgetitem77)
        __qtablewidgetitem78 = QTableWidgetItem()
        self.tableWidget_16.setVerticalHeaderItem(4, __qtablewidgetitem78)
        __qtablewidgetitem79 = QTableWidgetItem()
        self.tableWidget_16.setVerticalHeaderItem(5, __qtablewidgetitem79)
        __qtablewidgetitem80 = QTableWidgetItem()
        self.tableWidget_16.setVerticalHeaderItem(6, __qtablewidgetitem80)
        __qtablewidgetitem81 = QTableWidgetItem()
        self.tableWidget_16.setVerticalHeaderItem(7, __qtablewidgetitem81)
        __qtablewidgetitem82 = QTableWidgetItem()
        self.tableWidget_16.setItem(0, 0, __qtablewidgetitem82)
        __qtablewidgetitem83 = QTableWidgetItem()
        self.tableWidget_16.setItem(3, 0, __qtablewidgetitem83)
        self.tableWidget_16.setObjectName(u"tableWidget_16")
        self.tableWidget_16.setGeometry(QRect(340, 30, 121, 265))
        self.tableWidget_16.setMaximumSize(QSize(741, 16777215))
        self.Status.addTab(self.tab_2, "")
        self.Page = QWidget()
        self.Page.setObjectName(u"Page")
        self.tableWidget_17 = QTableWidget(self.Page)
        if (self.tableWidget_17.columnCount() < 1):
            self.tableWidget_17.setColumnCount(1)
        __qtablewidgetitem84 = QTableWidgetItem()
        self.tableWidget_17.setHorizontalHeaderItem(0, __qtablewidgetitem84)
        if (self.tableWidget_17.rowCount() < 8):
            self.tableWidget_17.setRowCount(8)
        __qtablewidgetitem85 = QTableWidgetItem()
        self.tableWidget_17.setVerticalHeaderItem(0, __qtablewidgetitem85)
        __qtablewidgetitem86 = QTableWidgetItem()
        self.tableWidget_17.setVerticalHeaderItem(1, __qtablewidgetitem86)
        __qtablewidgetitem87 = QTableWidgetItem()
        self.tableWidget_17.setVerticalHeaderItem(2, __qtablewidgetitem87)
        __qtablewidgetitem88 = QTableWidgetItem()
        self.tableWidget_17.setVerticalHeaderItem(3, __qtablewidgetitem88)
        __qtablewidgetitem89 = QTableWidgetItem()
        self.tableWidget_17.setVerticalHeaderItem(4, __qtablewidgetitem89)
        __qtablewidgetitem90 = QTableWidgetItem()
        self.tableWidget_17.setVerticalHeaderItem(5, __qtablewidgetitem90)
        __qtablewidgetitem91 = QTableWidgetItem()
        self.tableWidget_17.setVerticalHeaderItem(6, __qtablewidgetitem91)
        __qtablewidgetitem92 = QTableWidgetItem()
        self.tableWidget_17.setVerticalHeaderItem(7, __qtablewidgetitem92)
        __qtablewidgetitem93 = QTableWidgetItem()
        self.tableWidget_17.setItem(0, 0, __qtablewidgetitem93)
        __qtablewidgetitem94 = QTableWidgetItem()
        self.tableWidget_17.setItem(2, 0, __qtablewidgetitem94)
        self.tableWidget_17.setObjectName(u"tableWidget_17")
        self.tableWidget_17.setGeometry(QRect(600, 30, 121, 265))
        self.tableWidget_17.setMaximumSize(QSize(741, 16777215))
        self.tableWidget_18 = QTableWidget(self.Page)
        if (self.tableWidget_18.columnCount() < 1):
            self.tableWidget_18.setColumnCount(1)
        __qtablewidgetitem95 = QTableWidgetItem()
        self.tableWidget_18.setHorizontalHeaderItem(0, __qtablewidgetitem95)
        if (self.tableWidget_18.rowCount() < 8):
            self.tableWidget_18.setRowCount(8)
        __qtablewidgetitem96 = QTableWidgetItem()
        self.tableWidget_18.setVerticalHeaderItem(0, __qtablewidgetitem96)
        __qtablewidgetitem97 = QTableWidgetItem()
        self.tableWidget_18.setVerticalHeaderItem(1, __qtablewidgetitem97)
        __qtablewidgetitem98 = QTableWidgetItem()
        self.tableWidget_18.setVerticalHeaderItem(2, __qtablewidgetitem98)
        __qtablewidgetitem99 = QTableWidgetItem()
        self.tableWidget_18.setVerticalHeaderItem(3, __qtablewidgetitem99)
        __qtablewidgetitem100 = QTableWidgetItem()
        self.tableWidget_18.setVerticalHeaderItem(4, __qtablewidgetitem100)
        __qtablewidgetitem101 = QTableWidgetItem()
        self.tableWidget_18.setVerticalHeaderItem(5, __qtablewidgetitem101)
        __qtablewidgetitem102 = QTableWidgetItem()
        self.tableWidget_18.setVerticalHeaderItem(6, __qtablewidgetitem102)
        __qtablewidgetitem103 = QTableWidgetItem()
        self.tableWidget_18.setVerticalHeaderItem(7, __qtablewidgetitem103)
        __qtablewidgetitem104 = QTableWidgetItem()
        self.tableWidget_18.setItem(0, 0, __qtablewidgetitem104)
        self.tableWidget_18.setObjectName(u"tableWidget_18")
        self.tableWidget_18.setGeometry(QRect(190, 30, 141, 265))
        sizePolicy.setHeightForWidth(self.tableWidget_18.sizePolicy().hasHeightForWidth())
        self.tableWidget_18.setSizePolicy(sizePolicy)
        self.tableWidget_18.setMaximumSize(QSize(741, 16777215))
        self.tableWidget_19 = QTableWidget(self.Page)
        if (self.tableWidget_19.columnCount() < 1):
            self.tableWidget_19.setColumnCount(1)
        __qtablewidgetitem105 = QTableWidgetItem()
        self.tableWidget_19.setHorizontalHeaderItem(0, __qtablewidgetitem105)
        if (self.tableWidget_19.rowCount() < 8):
            self.tableWidget_19.setRowCount(8)
        __qtablewidgetitem106 = QTableWidgetItem()
        self.tableWidget_19.setVerticalHeaderItem(0, __qtablewidgetitem106)
        __qtablewidgetitem107 = QTableWidgetItem()
        self.tableWidget_19.setVerticalHeaderItem(1, __qtablewidgetitem107)
        __qtablewidgetitem108 = QTableWidgetItem()
        self.tableWidget_19.setVerticalHeaderItem(2, __qtablewidgetitem108)
        __qtablewidgetitem109 = QTableWidgetItem()
        self.tableWidget_19.setVerticalHeaderItem(3, __qtablewidgetitem109)
        __qtablewidgetitem110 = QTableWidgetItem()
        self.tableWidget_19.setVerticalHeaderItem(4, __qtablewidgetitem110)
        __qtablewidgetitem111 = QTableWidgetItem()
        self.tableWidget_19.setVerticalHeaderItem(5, __qtablewidgetitem111)
        __qtablewidgetitem112 = QTableWidgetItem()
        self.tableWidget_19.setVerticalHeaderItem(6, __qtablewidgetitem112)
        __qtablewidgetitem113 = QTableWidgetItem()
        self.tableWidget_19.setVerticalHeaderItem(7, __qtablewidgetitem113)
        __qtablewidgetitem114 = QTableWidgetItem()
        self.tableWidget_19.setItem(0, 0, __qtablewidgetitem114)
        self.tableWidget_19.setObjectName(u"tableWidget_19")
        self.tableWidget_19.setGeometry(QRect(470, 30, 121, 265))
        self.tableWidget_19.setMaximumSize(QSize(741, 16777215))
        self.tableWidget_20 = QTableWidget(self.Page)
        if (self.tableWidget_20.columnCount() < 1):
            self.tableWidget_20.setColumnCount(1)
        __qtablewidgetitem115 = QTableWidgetItem()
        self.tableWidget_20.setHorizontalHeaderItem(0, __qtablewidgetitem115)
        if (self.tableWidget_20.rowCount() < 8):
            self.tableWidget_20.setRowCount(8)
        __qtablewidgetitem116 = QTableWidgetItem()
        self.tableWidget_20.setVerticalHeaderItem(0, __qtablewidgetitem116)
        __qtablewidgetitem117 = QTableWidgetItem()
        self.tableWidget_20.setVerticalHeaderItem(1, __qtablewidgetitem117)
        __qtablewidgetitem118 = QTableWidgetItem()
        self.tableWidget_20.setVerticalHeaderItem(2, __qtablewidgetitem118)
        __qtablewidgetitem119 = QTableWidgetItem()
        self.tableWidget_20.setVerticalHeaderItem(3, __qtablewidgetitem119)
        __qtablewidgetitem120 = QTableWidgetItem()
        self.tableWidget_20.setVerticalHeaderItem(4, __qtablewidgetitem120)
        __qtablewidgetitem121 = QTableWidgetItem()
        self.tableWidget_20.setVerticalHeaderItem(5, __qtablewidgetitem121)
        __qtablewidgetitem122 = QTableWidgetItem()
        self.tableWidget_20.setVerticalHeaderItem(6, __qtablewidgetitem122)
        __qtablewidgetitem123 = QTableWidgetItem()
        self.tableWidget_20.setVerticalHeaderItem(7, __qtablewidgetitem123)
        __qtablewidgetitem124 = QTableWidgetItem()
        self.tableWidget_20.setItem(0, 0, __qtablewidgetitem124)
        __qtablewidgetitem125 = QTableWidgetItem()
        self.tableWidget_20.setItem(3, 0, __qtablewidgetitem125)
        self.tableWidget_20.setObjectName(u"tableWidget_20")
        self.tableWidget_20.setGeometry(QRect(340, 30, 121, 265))
        self.tableWidget_20.setMaximumSize(QSize(741, 16777215))
        self.Status.addTab(self.Page, "")
        self.tab_3 = QWidget()
        self.tab_3.setObjectName(u"tab_3")
        self.tableWidget_21 = QTableWidget(self.tab_3)
        if (self.tableWidget_21.columnCount() < 1):
            self.tableWidget_21.setColumnCount(1)
        __qtablewidgetitem126 = QTableWidgetItem()
        self.tableWidget_21.setHorizontalHeaderItem(0, __qtablewidgetitem126)
        if (self.tableWidget_21.rowCount() < 8):
            self.tableWidget_21.setRowCount(8)
        __qtablewidgetitem127 = QTableWidgetItem()
        self.tableWidget_21.setVerticalHeaderItem(0, __qtablewidgetitem127)
        __qtablewidgetitem128 = QTableWidgetItem()
        self.tableWidget_21.setVerticalHeaderItem(1, __qtablewidgetitem128)
        __qtablewidgetitem129 = QTableWidgetItem()
        self.tableWidget_21.setVerticalHeaderItem(2, __qtablewidgetitem129)
        __qtablewidgetitem130 = QTableWidgetItem()
        self.tableWidget_21.setVerticalHeaderItem(3, __qtablewidgetitem130)
        __qtablewidgetitem131 = QTableWidgetItem()
        self.tableWidget_21.setVerticalHeaderItem(4, __qtablewidgetitem131)
        __qtablewidgetitem132 = QTableWidgetItem()
        self.tableWidget_21.setVerticalHeaderItem(5, __qtablewidgetitem132)
        __qtablewidgetitem133 = QTableWidgetItem()
        self.tableWidget_21.setVerticalHeaderItem(6, __qtablewidgetitem133)
        __qtablewidgetitem134 = QTableWidgetItem()
        self.tableWidget_21.setVerticalHeaderItem(7, __qtablewidgetitem134)
        __qtablewidgetitem135 = QTableWidgetItem()
        self.tableWidget_21.setItem(0, 0, __qtablewidgetitem135)
        __qtablewidgetitem136 = QTableWidgetItem()
        self.tableWidget_21.setItem(2, 0, __qtablewidgetitem136)
        self.tableWidget_21.setObjectName(u"tableWidget_21")
        self.tableWidget_21.setGeometry(QRect(600, 30, 121, 265))
        self.tableWidget_21.setMaximumSize(QSize(741, 16777215))
        self.tableWidget_22 = QTableWidget(self.tab_3)
        if (self.tableWidget_22.columnCount() < 1):
            self.tableWidget_22.setColumnCount(1)
        __qtablewidgetitem137 = QTableWidgetItem()
        self.tableWidget_22.setHorizontalHeaderItem(0, __qtablewidgetitem137)
        if (self.tableWidget_22.rowCount() < 8):
            self.tableWidget_22.setRowCount(8)
        __qtablewidgetitem138 = QTableWidgetItem()
        self.tableWidget_22.setVerticalHeaderItem(0, __qtablewidgetitem138)
        __qtablewidgetitem139 = QTableWidgetItem()
        self.tableWidget_22.setVerticalHeaderItem(1, __qtablewidgetitem139)
        __qtablewidgetitem140 = QTableWidgetItem()
        self.tableWidget_22.setVerticalHeaderItem(2, __qtablewidgetitem140)
        __qtablewidgetitem141 = QTableWidgetItem()
        self.tableWidget_22.setVerticalHeaderItem(3, __qtablewidgetitem141)
        __qtablewidgetitem142 = QTableWidgetItem()
        self.tableWidget_22.setVerticalHeaderItem(4, __qtablewidgetitem142)
        __qtablewidgetitem143 = QTableWidgetItem()
        self.tableWidget_22.setVerticalHeaderItem(5, __qtablewidgetitem143)
        __qtablewidgetitem144 = QTableWidgetItem()
        self.tableWidget_22.setVerticalHeaderItem(6, __qtablewidgetitem144)
        __qtablewidgetitem145 = QTableWidgetItem()
        self.tableWidget_22.setVerticalHeaderItem(7, __qtablewidgetitem145)
        __qtablewidgetitem146 = QTableWidgetItem()
        self.tableWidget_22.setItem(0, 0, __qtablewidgetitem146)
        self.tableWidget_22.setObjectName(u"tableWidget_22")
        self.tableWidget_22.setGeometry(QRect(190, 30, 141, 265))
        sizePolicy.setHeightForWidth(self.tableWidget_22.sizePolicy().hasHeightForWidth())
        self.tableWidget_22.setSizePolicy(sizePolicy)
        self.tableWidget_22.setMaximumSize(QSize(741, 16777215))
        self.tableWidget_23 = QTableWidget(self.tab_3)
        if (self.tableWidget_23.columnCount() < 1):
            self.tableWidget_23.setColumnCount(1)
        __qtablewidgetitem147 = QTableWidgetItem()
        self.tableWidget_23.setHorizontalHeaderItem(0, __qtablewidgetitem147)
        if (self.tableWidget_23.rowCount() < 8):
            self.tableWidget_23.setRowCount(8)
        __qtablewidgetitem148 = QTableWidgetItem()
        self.tableWidget_23.setVerticalHeaderItem(0, __qtablewidgetitem148)
        __qtablewidgetitem149 = QTableWidgetItem()
        self.tableWidget_23.setVerticalHeaderItem(1, __qtablewidgetitem149)
        __qtablewidgetitem150 = QTableWidgetItem()
        self.tableWidget_23.setVerticalHeaderItem(2, __qtablewidgetitem150)
        __qtablewidgetitem151 = QTableWidgetItem()
        self.tableWidget_23.setVerticalHeaderItem(3, __qtablewidgetitem151)
        __qtablewidgetitem152 = QTableWidgetItem()
        self.tableWidget_23.setVerticalHeaderItem(4, __qtablewidgetitem152)
        __qtablewidgetitem153 = QTableWidgetItem()
        self.tableWidget_23.setVerticalHeaderItem(5, __qtablewidgetitem153)
        __qtablewidgetitem154 = QTableWidgetItem()
        self.tableWidget_23.setVerticalHeaderItem(6, __qtablewidgetitem154)
        __qtablewidgetitem155 = QTableWidgetItem()
        self.tableWidget_23.setVerticalHeaderItem(7, __qtablewidgetitem155)
        __qtablewidgetitem156 = QTableWidgetItem()
        self.tableWidget_23.setItem(0, 0, __qtablewidgetitem156)
        self.tableWidget_23.setObjectName(u"tableWidget_23")
        self.tableWidget_23.setGeometry(QRect(470, 30, 121, 265))
        self.tableWidget_23.setMaximumSize(QSize(741, 16777215))
        self.tableWidget_24 = QTableWidget(self.tab_3)
        if (self.tableWidget_24.columnCount() < 1):
            self.tableWidget_24.setColumnCount(1)
        __qtablewidgetitem157 = QTableWidgetItem()
        self.tableWidget_24.setHorizontalHeaderItem(0, __qtablewidgetitem157)
        if (self.tableWidget_24.rowCount() < 8):
            self.tableWidget_24.setRowCount(8)
        __qtablewidgetitem158 = QTableWidgetItem()
        self.tableWidget_24.setVerticalHeaderItem(0, __qtablewidgetitem158)
        __qtablewidgetitem159 = QTableWidgetItem()
        self.tableWidget_24.setVerticalHeaderItem(1, __qtablewidgetitem159)
        __qtablewidgetitem160 = QTableWidgetItem()
        self.tableWidget_24.setVerticalHeaderItem(2, __qtablewidgetitem160)
        __qtablewidgetitem161 = QTableWidgetItem()
        self.tableWidget_24.setVerticalHeaderItem(3, __qtablewidgetitem161)
        __qtablewidgetitem162 = QTableWidgetItem()
        self.tableWidget_24.setVerticalHeaderItem(4, __qtablewidgetitem162)
        __qtablewidgetitem163 = QTableWidgetItem()
        self.tableWidget_24.setVerticalHeaderItem(5, __qtablewidgetitem163)
        __qtablewidgetitem164 = QTableWidgetItem()
        self.tableWidget_24.setVerticalHeaderItem(6, __qtablewidgetitem164)
        __qtablewidgetitem165 = QTableWidgetItem()
        self.tableWidget_24.setVerticalHeaderItem(7, __qtablewidgetitem165)
        __qtablewidgetitem166 = QTableWidgetItem()
        self.tableWidget_24.setItem(0, 0, __qtablewidgetitem166)
        __qtablewidgetitem167 = QTableWidgetItem()
        self.tableWidget_24.setItem(3, 0, __qtablewidgetitem167)
        self.tableWidget_24.setObjectName(u"tableWidget_24")
        self.tableWidget_24.setGeometry(QRect(340, 30, 121, 265))
        self.tableWidget_24.setMaximumSize(QSize(741, 16777215))
        self.Status.addTab(self.tab_3, "")

        self.verticalLayout_2.addWidget(self.Status)


        self.gridLayout.addLayout(self.verticalLayout_2, 1, 0, 1, 1)

        self.frame = QFrame(self.centralwidget)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.frame)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.label = QLabel(self.frame)
        self.label.setObjectName(u"label")

        self.horizontalLayout.addWidget(self.label)

        self.comboBox = QComboBox(self.frame)
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.setObjectName(u"comboBox")
        self.comboBox.model().item(1).setEnabled(False)
        self.comboBox.model().item(2).setEnabled(False)


        self.horizontalLayout.addWidget(self.comboBox)


        self.gridLayout.addWidget(self.frame, 0, 0, 1, 1)

        Visualisierung.setCentralWidget(self.centralwidget)
        self.statusbar = QStatusBar(Visualisierung)
        self.statusbar.setObjectName(u"statusbar")
        Visualisierung.setStatusBar(self.statusbar)
        self.menubar = QMenuBar(Visualisierung)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 977, 21))
        self.menuStatus = QMenu(self.menubar)
        self.menuStatus.setObjectName(u"menuStatus")
        Visualisierung.setMenuBar(self.menubar)

        self.menubar.addAction(self.menuStatus.menuAction())
        self.menuStatus.addSeparator()
        self.menuStatus.addAction(self.importAction)

        self.retranslateUi(Visualisierung)

        self.Status.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(Visualisierung)
    # setupUi

    def retranslateUi(self, Visualisierung):
        Visualisierung.setWindowTitle(QCoreApplication.translate("Visualisierung", u"Visualisierung", None))
        self.importAction.setText(QCoreApplication.translate("Visualisierung", u"Open...", None))
        ___qtablewidgetitem = self.tableWidget.verticalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("Visualisierung", u"Automatik", None));
        ___qtablewidgetitem1 = self.tableWidget.verticalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("Visualisierung", u"Hand", None));
        ___qtablewidgetitem2 = self.tableWidget.verticalHeaderItem(2)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("Visualisierung", u"F\u00e4hrt vorw\u00e4rts", None));
        ___qtablewidgetitem3 = self.tableWidget.verticalHeaderItem(3)
        ___qtablewidgetitem3.setText(QCoreApplication.translate("Visualisierung", u"F\u00e4hrt r\u00fcckw\u00e4rts", None));
        ___qtablewidgetitem4 = self.tableWidget.verticalHeaderItem(4)
        ___qtablewidgetitem4.setText(QCoreApplication.translate("Visualisierung", u"Teilautomatik", None));
        ___qtablewidgetitem5 = self.tableWidget.verticalHeaderItem(5)
        ___qtablewidgetitem5.setText(QCoreApplication.translate("Visualisierung", u"Betriebsautomatik", None));
        ___qtablewidgetitem6 = self.tableWidget.verticalHeaderItem(6)
        ___qtablewidgetitem6.setText(QCoreApplication.translate("Visualisierung", u"Muli 1 in Pos.", None));
        ___qtablewidgetitem7 = self.tableWidget.verticalHeaderItem(7)
        ___qtablewidgetitem7.setText(QCoreApplication.translate("Visualisierung", u"Muli 2 in Pos.", None));

        __sortingEnabled = self.tableWidget.isSortingEnabled()
        self.tableWidget.setSortingEnabled(False)
        ___qtablewidgetitem8 = self.tableWidget.item(0, 0)
        ___qtablewidgetitem8.setText(QCoreApplication.translate("Visualisierung", u"X", None));
        self.tableWidget.setSortingEnabled(__sortingEnabled)

        ___qtablewidgetitem9 = self.tableWidget_2.verticalHeaderItem(0)
        ___qtablewidgetitem9.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1M leer", None));
        ___qtablewidgetitem10 = self.tableWidget_2.verticalHeaderItem(1)
        ___qtablewidgetitem10.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2M leer", None));
        ___qtablewidgetitem11 = self.tableWidget_2.verticalHeaderItem(2)
        ___qtablewidgetitem11.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3M leer", None));
        ___qtablewidgetitem12 = self.tableWidget_2.verticalHeaderItem(3)
        ___qtablewidgetitem12.setText(QCoreApplication.translate("Visualisierung", u"Bahn 4M leer", None));
        ___qtablewidgetitem13 = self.tableWidget_2.verticalHeaderItem(4)
        ___qtablewidgetitem13.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1+2M leer", None));
        ___qtablewidgetitem14 = self.tableWidget_2.verticalHeaderItem(5)
        ___qtablewidgetitem14.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2+3M leer", None));
        ___qtablewidgetitem15 = self.tableWidget_2.verticalHeaderItem(6)
        ___qtablewidgetitem15.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3+4M leer", None));
        ___qtablewidgetitem16 = self.tableWidget_2.verticalHeaderItem(7)
        ___qtablewidgetitem16.setText(QCoreApplication.translate("Visualisierung", u"Reserviert", None));

        __sortingEnabled1 = self.tableWidget_2.isSortingEnabled()
        self.tableWidget_2.setSortingEnabled(False)
        ___qtablewidgetitem17 = self.tableWidget_2.item(3, 0)
        ___qtablewidgetitem17.setText(QCoreApplication.translate("Visualisierung", u"X", None));
        self.tableWidget_2.setSortingEnabled(__sortingEnabled1)

        ___qtablewidgetitem18 = self.tableWidget_4.verticalHeaderItem(0)
        ___qtablewidgetitem18.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1R leer", None));
        ___qtablewidgetitem19 = self.tableWidget_4.verticalHeaderItem(1)
        ___qtablewidgetitem19.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2R leer", None));
        ___qtablewidgetitem20 = self.tableWidget_4.verticalHeaderItem(2)
        ___qtablewidgetitem20.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3R leer", None));
        ___qtablewidgetitem21 = self.tableWidget_4.verticalHeaderItem(3)
        ___qtablewidgetitem21.setText(QCoreApplication.translate("Visualisierung", u"Bahn 4R leer", None));
        ___qtablewidgetitem22 = self.tableWidget_4.verticalHeaderItem(4)
        ___qtablewidgetitem22.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1+2R leer", None));
        ___qtablewidgetitem23 = self.tableWidget_4.verticalHeaderItem(5)
        ___qtablewidgetitem23.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2+3R leer", None));
        ___qtablewidgetitem24 = self.tableWidget_4.verticalHeaderItem(6)
        ___qtablewidgetitem24.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3+4R leer", None));
        ___qtablewidgetitem25 = self.tableWidget_4.verticalHeaderItem(7)
        ___qtablewidgetitem25.setText(QCoreApplication.translate("Visualisierung", u"Reserviert", None));

        __sortingEnabled2 = self.tableWidget_4.isSortingEnabled()
        self.tableWidget_4.setSortingEnabled(False)
        ___qtablewidgetitem26 = self.tableWidget_4.item(0, 0)
        ___qtablewidgetitem26.setText(QCoreApplication.translate("Visualisierung", u"X", None));
        self.tableWidget_4.setSortingEnabled(__sortingEnabled2)

        ___qtablewidgetitem27 = self.tableWidget_3.verticalHeaderItem(0)
        ___qtablewidgetitem27.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1L leer", None));
        ___qtablewidgetitem28 = self.tableWidget_3.verticalHeaderItem(1)
        ___qtablewidgetitem28.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2L leer", None));
        ___qtablewidgetitem29 = self.tableWidget_3.verticalHeaderItem(2)
        ___qtablewidgetitem29.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3L leer", None));
        ___qtablewidgetitem30 = self.tableWidget_3.verticalHeaderItem(3)
        ___qtablewidgetitem30.setText(QCoreApplication.translate("Visualisierung", u"Bahn 4L leer", None));
        ___qtablewidgetitem31 = self.tableWidget_3.verticalHeaderItem(4)
        ___qtablewidgetitem31.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1+2L leer", None));
        ___qtablewidgetitem32 = self.tableWidget_3.verticalHeaderItem(5)
        ___qtablewidgetitem32.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2+3L leer", None));
        ___qtablewidgetitem33 = self.tableWidget_3.verticalHeaderItem(6)
        ___qtablewidgetitem33.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3+4L leer", None));
        ___qtablewidgetitem34 = self.tableWidget_3.verticalHeaderItem(7)
        ___qtablewidgetitem34.setText(QCoreApplication.translate("Visualisierung", u"Reserviert", None));

        __sortingEnabled3 = self.tableWidget_3.isSortingEnabled()
        self.tableWidget_3.setSortingEnabled(False)
        ___qtablewidgetitem35 = self.tableWidget_3.item(2, 0)
        ___qtablewidgetitem35.setText(QCoreApplication.translate("Visualisierung", u"X", None));
        self.tableWidget_3.setSortingEnabled(__sortingEnabled3)

        self.Status.setTabText(self.Status.indexOf(self.tab), QCoreApplication.translate("Visualisierung", u"Status", None))
        ___qtablewidgetitem36 = self.tableWidget_13.verticalHeaderItem(0)
        ___qtablewidgetitem36.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1L leer", None));
        ___qtablewidgetitem37 = self.tableWidget_13.verticalHeaderItem(1)
        ___qtablewidgetitem37.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2L leer", None));
        ___qtablewidgetitem38 = self.tableWidget_13.verticalHeaderItem(2)
        ___qtablewidgetitem38.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3L leer", None));
        ___qtablewidgetitem39 = self.tableWidget_13.verticalHeaderItem(3)
        ___qtablewidgetitem39.setText(QCoreApplication.translate("Visualisierung", u"Bahn 4L leer", None));
        ___qtablewidgetitem40 = self.tableWidget_13.verticalHeaderItem(4)
        ___qtablewidgetitem40.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1+2L leer", None));
        ___qtablewidgetitem41 = self.tableWidget_13.verticalHeaderItem(5)
        ___qtablewidgetitem41.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2+3L leer", None));
        ___qtablewidgetitem42 = self.tableWidget_13.verticalHeaderItem(6)
        ___qtablewidgetitem42.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3+4L leer", None));
        ___qtablewidgetitem43 = self.tableWidget_13.verticalHeaderItem(7)
        ___qtablewidgetitem43.setText(QCoreApplication.translate("Visualisierung", u"Reserviert", None));

        __sortingEnabled4 = self.tableWidget_13.isSortingEnabled()
        self.tableWidget_13.setSortingEnabled(False)
        ___qtablewidgetitem44 = self.tableWidget_13.item(2, 0)
        ___qtablewidgetitem44.setText(QCoreApplication.translate("Visualisierung", u"X", None));
        self.tableWidget_13.setSortingEnabled(__sortingEnabled4)

        ___qtablewidgetitem45 = self.tableWidget_14.verticalHeaderItem(0)
        ___qtablewidgetitem45.setText(QCoreApplication.translate("Visualisierung", u"Automatik", None));
        ___qtablewidgetitem46 = self.tableWidget_14.verticalHeaderItem(1)
        ___qtablewidgetitem46.setText(QCoreApplication.translate("Visualisierung", u"Hand", None));
        ___qtablewidgetitem47 = self.tableWidget_14.verticalHeaderItem(2)
        ___qtablewidgetitem47.setText(QCoreApplication.translate("Visualisierung", u"F\u00e4hrt vorw\u00e4rts", None));
        ___qtablewidgetitem48 = self.tableWidget_14.verticalHeaderItem(3)
        ___qtablewidgetitem48.setText(QCoreApplication.translate("Visualisierung", u"F\u00e4hrt r\u00fcckw\u00e4rts", None));
        ___qtablewidgetitem49 = self.tableWidget_14.verticalHeaderItem(4)
        ___qtablewidgetitem49.setText(QCoreApplication.translate("Visualisierung", u"Teilautomatik", None));
        ___qtablewidgetitem50 = self.tableWidget_14.verticalHeaderItem(5)
        ___qtablewidgetitem50.setText(QCoreApplication.translate("Visualisierung", u"Betriebsautomatik", None));
        ___qtablewidgetitem51 = self.tableWidget_14.verticalHeaderItem(6)
        ___qtablewidgetitem51.setText(QCoreApplication.translate("Visualisierung", u"Muli 1 in Pos.", None));
        ___qtablewidgetitem52 = self.tableWidget_14.verticalHeaderItem(7)
        ___qtablewidgetitem52.setText(QCoreApplication.translate("Visualisierung", u"Muli 2 in Pos.", None));

        __sortingEnabled5 = self.tableWidget_14.isSortingEnabled()
        self.tableWidget_14.setSortingEnabled(False)
        ___qtablewidgetitem53 = self.tableWidget_14.item(0, 0)
        ___qtablewidgetitem53.setText(QCoreApplication.translate("Visualisierung", u"X", None));
        self.tableWidget_14.setSortingEnabled(__sortingEnabled5)

        ___qtablewidgetitem54 = self.tableWidget_15.verticalHeaderItem(0)
        ___qtablewidgetitem54.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1R leer", None));
        ___qtablewidgetitem55 = self.tableWidget_15.verticalHeaderItem(1)
        ___qtablewidgetitem55.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2R leer", None));
        ___qtablewidgetitem56 = self.tableWidget_15.verticalHeaderItem(2)
        ___qtablewidgetitem56.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3R leer", None));
        ___qtablewidgetitem57 = self.tableWidget_15.verticalHeaderItem(3)
        ___qtablewidgetitem57.setText(QCoreApplication.translate("Visualisierung", u"Bahn 4R leer", None));
        ___qtablewidgetitem58 = self.tableWidget_15.verticalHeaderItem(4)
        ___qtablewidgetitem58.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1+2R leer", None));
        ___qtablewidgetitem59 = self.tableWidget_15.verticalHeaderItem(5)
        ___qtablewidgetitem59.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2+3R leer", None));
        ___qtablewidgetitem60 = self.tableWidget_15.verticalHeaderItem(6)
        ___qtablewidgetitem60.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3+4R leer", None));
        ___qtablewidgetitem61 = self.tableWidget_15.verticalHeaderItem(7)
        ___qtablewidgetitem61.setText(QCoreApplication.translate("Visualisierung", u"Reserviert", None));

        __sortingEnabled6 = self.tableWidget_15.isSortingEnabled()
        self.tableWidget_15.setSortingEnabled(False)
        ___qtablewidgetitem62 = self.tableWidget_15.item(0, 0)
        ___qtablewidgetitem62.setText(QCoreApplication.translate("Visualisierung", u"X", None));
        self.tableWidget_15.setSortingEnabled(__sortingEnabled6)

        ___qtablewidgetitem63 = self.tableWidget_16.verticalHeaderItem(0)
        ___qtablewidgetitem63.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1M leer", None));
        ___qtablewidgetitem64 = self.tableWidget_16.verticalHeaderItem(1)
        ___qtablewidgetitem64.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2M leer", None));
        ___qtablewidgetitem65 = self.tableWidget_16.verticalHeaderItem(2)
        ___qtablewidgetitem65.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3M leer", None));
        ___qtablewidgetitem66 = self.tableWidget_16.verticalHeaderItem(3)
        ___qtablewidgetitem66.setText(QCoreApplication.translate("Visualisierung", u"Bahn 4M leer", None));
        ___qtablewidgetitem67 = self.tableWidget_16.verticalHeaderItem(4)
        ___qtablewidgetitem67.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1+2M leer", None));
        ___qtablewidgetitem68 = self.tableWidget_16.verticalHeaderItem(5)
        ___qtablewidgetitem68.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2+3M leer", None));
        ___qtablewidgetitem69 = self.tableWidget_16.verticalHeaderItem(6)
        ___qtablewidgetitem69.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3+4M leer", None));
        ___qtablewidgetitem70 = self.tableWidget_16.verticalHeaderItem(7)
        ___qtablewidgetitem70.setText(QCoreApplication.translate("Visualisierung", u"Reserviert", None));

        __sortingEnabled7 = self.tableWidget_16.isSortingEnabled()
        self.tableWidget_16.setSortingEnabled(False)
        ___qtablewidgetitem71 = self.tableWidget_16.item(3, 0)
        ___qtablewidgetitem71.setText(QCoreApplication.translate("Visualisierung", u"X", None));
        self.tableWidget_16.setSortingEnabled(__sortingEnabled7)

        self.Status.setTabText(self.Status.indexOf(self.tab_2), QCoreApplication.translate("Visualisierung", u"Function", None))
        ___qtablewidgetitem72 = self.tableWidget_17.verticalHeaderItem(0)
        ___qtablewidgetitem72.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1L leer", None));
        ___qtablewidgetitem73 = self.tableWidget_17.verticalHeaderItem(1)
        ___qtablewidgetitem73.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2L leer", None));
        ___qtablewidgetitem74 = self.tableWidget_17.verticalHeaderItem(2)
        ___qtablewidgetitem74.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3L leer", None));
        ___qtablewidgetitem75 = self.tableWidget_17.verticalHeaderItem(3)
        ___qtablewidgetitem75.setText(QCoreApplication.translate("Visualisierung", u"Bahn 4L leer", None));
        ___qtablewidgetitem76 = self.tableWidget_17.verticalHeaderItem(4)
        ___qtablewidgetitem76.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1+2L leer", None));
        ___qtablewidgetitem77 = self.tableWidget_17.verticalHeaderItem(5)
        ___qtablewidgetitem77.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2+3L leer", None));
        ___qtablewidgetitem78 = self.tableWidget_17.verticalHeaderItem(6)
        ___qtablewidgetitem78.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3+4L leer", None));
        ___qtablewidgetitem79 = self.tableWidget_17.verticalHeaderItem(7)
        ___qtablewidgetitem79.setText(QCoreApplication.translate("Visualisierung", u"Reserviert", None));

        __sortingEnabled8 = self.tableWidget_17.isSortingEnabled()
        self.tableWidget_17.setSortingEnabled(False)
        ___qtablewidgetitem80 = self.tableWidget_17.item(2, 0)
        ___qtablewidgetitem80.setText(QCoreApplication.translate("Visualisierung", u"X", None));
        self.tableWidget_17.setSortingEnabled(__sortingEnabled8)

        ___qtablewidgetitem81 = self.tableWidget_18.verticalHeaderItem(0)
        ___qtablewidgetitem81.setText(QCoreApplication.translate("Visualisierung", u"Automatik", None));
        ___qtablewidgetitem82 = self.tableWidget_18.verticalHeaderItem(1)
        ___qtablewidgetitem82.setText(QCoreApplication.translate("Visualisierung", u"Hand", None));
        ___qtablewidgetitem83 = self.tableWidget_18.verticalHeaderItem(2)
        ___qtablewidgetitem83.setText(QCoreApplication.translate("Visualisierung", u"F\u00e4hrt vorw\u00e4rts", None));
        ___qtablewidgetitem84 = self.tableWidget_18.verticalHeaderItem(3)
        ___qtablewidgetitem84.setText(QCoreApplication.translate("Visualisierung", u"F\u00e4hrt r\u00fcckw\u00e4rts", None));
        ___qtablewidgetitem85 = self.tableWidget_18.verticalHeaderItem(4)
        ___qtablewidgetitem85.setText(QCoreApplication.translate("Visualisierung", u"Teilautomatik", None));
        ___qtablewidgetitem86 = self.tableWidget_18.verticalHeaderItem(5)
        ___qtablewidgetitem86.setText(QCoreApplication.translate("Visualisierung", u"Betriebsautomatik", None));
        ___qtablewidgetitem87 = self.tableWidget_18.verticalHeaderItem(6)
        ___qtablewidgetitem87.setText(QCoreApplication.translate("Visualisierung", u"Muli 1 in Pos.", None));
        ___qtablewidgetitem88 = self.tableWidget_18.verticalHeaderItem(7)
        ___qtablewidgetitem88.setText(QCoreApplication.translate("Visualisierung", u"Muli 2 in Pos.", None));

        __sortingEnabled9 = self.tableWidget_18.isSortingEnabled()
        self.tableWidget_18.setSortingEnabled(False)
        ___qtablewidgetitem89 = self.tableWidget_18.item(0, 0)
        ___qtablewidgetitem89.setText(QCoreApplication.translate("Visualisierung", u"X", None));
        self.tableWidget_18.setSortingEnabled(__sortingEnabled9)

        ___qtablewidgetitem90 = self.tableWidget_19.verticalHeaderItem(0)
        ___qtablewidgetitem90.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1R leer", None));
        ___qtablewidgetitem91 = self.tableWidget_19.verticalHeaderItem(1)
        ___qtablewidgetitem91.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2R leer", None));
        ___qtablewidgetitem92 = self.tableWidget_19.verticalHeaderItem(2)
        ___qtablewidgetitem92.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3R leer", None));
        ___qtablewidgetitem93 = self.tableWidget_19.verticalHeaderItem(3)
        ___qtablewidgetitem93.setText(QCoreApplication.translate("Visualisierung", u"Bahn 4R leer", None));
        ___qtablewidgetitem94 = self.tableWidget_19.verticalHeaderItem(4)
        ___qtablewidgetitem94.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1+2R leer", None));
        ___qtablewidgetitem95 = self.tableWidget_19.verticalHeaderItem(5)
        ___qtablewidgetitem95.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2+3R leer", None));
        ___qtablewidgetitem96 = self.tableWidget_19.verticalHeaderItem(6)
        ___qtablewidgetitem96.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3+4R leer", None));
        ___qtablewidgetitem97 = self.tableWidget_19.verticalHeaderItem(7)
        ___qtablewidgetitem97.setText(QCoreApplication.translate("Visualisierung", u"Reserviert", None));

        __sortingEnabled10 = self.tableWidget_19.isSortingEnabled()
        self.tableWidget_19.setSortingEnabled(False)
        ___qtablewidgetitem98 = self.tableWidget_19.item(0, 0)
        ___qtablewidgetitem98.setText(QCoreApplication.translate("Visualisierung", u"X", None));
        self.tableWidget_19.setSortingEnabled(__sortingEnabled10)

        ___qtablewidgetitem99 = self.tableWidget_20.verticalHeaderItem(0)
        ___qtablewidgetitem99.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1M leer", None));
        ___qtablewidgetitem100 = self.tableWidget_20.verticalHeaderItem(1)
        ___qtablewidgetitem100.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2M leer", None));
        ___qtablewidgetitem101 = self.tableWidget_20.verticalHeaderItem(2)
        ___qtablewidgetitem101.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3M leer", None));
        ___qtablewidgetitem102 = self.tableWidget_20.verticalHeaderItem(3)
        ___qtablewidgetitem102.setText(QCoreApplication.translate("Visualisierung", u"Bahn 4M leer", None));
        ___qtablewidgetitem103 = self.tableWidget_20.verticalHeaderItem(4)
        ___qtablewidgetitem103.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1+2M leer", None));
        ___qtablewidgetitem104 = self.tableWidget_20.verticalHeaderItem(5)
        ___qtablewidgetitem104.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2+3M leer", None));
        ___qtablewidgetitem105 = self.tableWidget_20.verticalHeaderItem(6)
        ___qtablewidgetitem105.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3+4M leer", None));
        ___qtablewidgetitem106 = self.tableWidget_20.verticalHeaderItem(7)
        ___qtablewidgetitem106.setText(QCoreApplication.translate("Visualisierung", u"Reserviert", None));

        __sortingEnabled11 = self.tableWidget_20.isSortingEnabled()
        self.tableWidget_20.setSortingEnabled(False)
        ___qtablewidgetitem107 = self.tableWidget_20.item(3, 0)
        ___qtablewidgetitem107.setText(QCoreApplication.translate("Visualisierung", u"X", None));
        self.tableWidget_20.setSortingEnabled(__sortingEnabled11)

        self.Status.setTabText(self.Status.indexOf(self.Page), QCoreApplication.translate("Visualisierung", u"Function-DD", None))
        ___qtablewidgetitem108 = self.tableWidget_21.verticalHeaderItem(0)
        ___qtablewidgetitem108.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1L leer", None));
        ___qtablewidgetitem109 = self.tableWidget_21.verticalHeaderItem(1)
        ___qtablewidgetitem109.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2L leer", None));
        ___qtablewidgetitem110 = self.tableWidget_21.verticalHeaderItem(2)
        ___qtablewidgetitem110.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3L leer", None));
        ___qtablewidgetitem111 = self.tableWidget_21.verticalHeaderItem(3)
        ___qtablewidgetitem111.setText(QCoreApplication.translate("Visualisierung", u"Bahn 4L leer", None));
        ___qtablewidgetitem112 = self.tableWidget_21.verticalHeaderItem(4)
        ___qtablewidgetitem112.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1+2L leer", None));
        ___qtablewidgetitem113 = self.tableWidget_21.verticalHeaderItem(5)
        ___qtablewidgetitem113.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2+3L leer", None));
        ___qtablewidgetitem114 = self.tableWidget_21.verticalHeaderItem(6)
        ___qtablewidgetitem114.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3+4L leer", None));
        ___qtablewidgetitem115 = self.tableWidget_21.verticalHeaderItem(7)
        ___qtablewidgetitem115.setText(QCoreApplication.translate("Visualisierung", u"Reserviert", None));

        __sortingEnabled12 = self.tableWidget_21.isSortingEnabled()
        self.tableWidget_21.setSortingEnabled(False)
        ___qtablewidgetitem116 = self.tableWidget_21.item(2, 0)
        ___qtablewidgetitem116.setText(QCoreApplication.translate("Visualisierung", u"X", None));
        self.tableWidget_21.setSortingEnabled(__sortingEnabled12)

        ___qtablewidgetitem117 = self.tableWidget_22.verticalHeaderItem(0)
        ___qtablewidgetitem117.setText(QCoreApplication.translate("Visualisierung", u"Automatik", None));
        ___qtablewidgetitem118 = self.tableWidget_22.verticalHeaderItem(1)
        ___qtablewidgetitem118.setText(QCoreApplication.translate("Visualisierung", u"Hand", None));
        ___qtablewidgetitem119 = self.tableWidget_22.verticalHeaderItem(2)
        ___qtablewidgetitem119.setText(QCoreApplication.translate("Visualisierung", u"F\u00e4hrt vorw\u00e4rts", None));
        ___qtablewidgetitem120 = self.tableWidget_22.verticalHeaderItem(3)
        ___qtablewidgetitem120.setText(QCoreApplication.translate("Visualisierung", u"F\u00e4hrt r\u00fcckw\u00e4rts", None));
        ___qtablewidgetitem121 = self.tableWidget_22.verticalHeaderItem(4)
        ___qtablewidgetitem121.setText(QCoreApplication.translate("Visualisierung", u"Teilautomatik", None));
        ___qtablewidgetitem122 = self.tableWidget_22.verticalHeaderItem(5)
        ___qtablewidgetitem122.setText(QCoreApplication.translate("Visualisierung", u"Betriebsautomatik", None));
        ___qtablewidgetitem123 = self.tableWidget_22.verticalHeaderItem(6)
        ___qtablewidgetitem123.setText(QCoreApplication.translate("Visualisierung", u"Muli 1 in Pos.", None));
        ___qtablewidgetitem124 = self.tableWidget_22.verticalHeaderItem(7)
        ___qtablewidgetitem124.setText(QCoreApplication.translate("Visualisierung", u"Muli 2 in Pos.", None));

        __sortingEnabled13 = self.tableWidget_22.isSortingEnabled()
        self.tableWidget_22.setSortingEnabled(False)
        ___qtablewidgetitem125 = self.tableWidget_22.item(0, 0)
        ___qtablewidgetitem125.setText(QCoreApplication.translate("Visualisierung", u"X", None));
        self.tableWidget_22.setSortingEnabled(__sortingEnabled13)

        ___qtablewidgetitem126 = self.tableWidget_23.verticalHeaderItem(0)
        ___qtablewidgetitem126.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1R leer", None));
        ___qtablewidgetitem127 = self.tableWidget_23.verticalHeaderItem(1)
        ___qtablewidgetitem127.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2R leer", None));
        ___qtablewidgetitem128 = self.tableWidget_23.verticalHeaderItem(2)
        ___qtablewidgetitem128.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3R leer", None));
        ___qtablewidgetitem129 = self.tableWidget_23.verticalHeaderItem(3)
        ___qtablewidgetitem129.setText(QCoreApplication.translate("Visualisierung", u"Bahn 4R leer", None));
        ___qtablewidgetitem130 = self.tableWidget_23.verticalHeaderItem(4)
        ___qtablewidgetitem130.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1+2R leer", None));
        ___qtablewidgetitem131 = self.tableWidget_23.verticalHeaderItem(5)
        ___qtablewidgetitem131.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2+3R leer", None));
        ___qtablewidgetitem132 = self.tableWidget_23.verticalHeaderItem(6)
        ___qtablewidgetitem132.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3+4R leer", None));
        ___qtablewidgetitem133 = self.tableWidget_23.verticalHeaderItem(7)
        ___qtablewidgetitem133.setText(QCoreApplication.translate("Visualisierung", u"Reserviert", None));

        __sortingEnabled14 = self.tableWidget_23.isSortingEnabled()
        self.tableWidget_23.setSortingEnabled(False)
        ___qtablewidgetitem134 = self.tableWidget_23.item(0, 0)
        ___qtablewidgetitem134.setText(QCoreApplication.translate("Visualisierung", u"X", None));
        self.tableWidget_23.setSortingEnabled(__sortingEnabled14)

        ___qtablewidgetitem135 = self.tableWidget_24.verticalHeaderItem(0)
        ___qtablewidgetitem135.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1M leer", None));
        ___qtablewidgetitem136 = self.tableWidget_24.verticalHeaderItem(1)
        ___qtablewidgetitem136.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2M leer", None));
        ___qtablewidgetitem137 = self.tableWidget_24.verticalHeaderItem(2)
        ___qtablewidgetitem137.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3M leer", None));
        ___qtablewidgetitem138 = self.tableWidget_24.verticalHeaderItem(3)
        ___qtablewidgetitem138.setText(QCoreApplication.translate("Visualisierung", u"Bahn 4M leer", None));
        ___qtablewidgetitem139 = self.tableWidget_24.verticalHeaderItem(4)
        ___qtablewidgetitem139.setText(QCoreApplication.translate("Visualisierung", u"Bahn 1+2M leer", None));
        ___qtablewidgetitem140 = self.tableWidget_24.verticalHeaderItem(5)
        ___qtablewidgetitem140.setText(QCoreApplication.translate("Visualisierung", u"Bahn 2+3M leer", None));
        ___qtablewidgetitem141 = self.tableWidget_24.verticalHeaderItem(6)
        ___qtablewidgetitem141.setText(QCoreApplication.translate("Visualisierung", u"Bahn 3+4M leer", None));
        ___qtablewidgetitem142 = self.tableWidget_24.verticalHeaderItem(7)
        ___qtablewidgetitem142.setText(QCoreApplication.translate("Visualisierung", u"Reserviert", None));

        __sortingEnabled15 = self.tableWidget_24.isSortingEnabled()
        self.tableWidget_24.setSortingEnabled(False)
        ___qtablewidgetitem143 = self.tableWidget_24.item(3, 0)
        ___qtablewidgetitem143.setText(QCoreApplication.translate("Visualisierung", u"X", None));
        self.tableWidget_24.setSortingEnabled(__sortingEnabled15)

        self.Status.setTabText(self.Status.indexOf(self.tab_3), QCoreApplication.translate("Visualisierung", u"Errors", None))
        self.label.setText(QCoreApplication.translate("Visualisierung", u"Filter by...", None))
        self.comboBox.setItemText(0, QCoreApplication.translate("Visualisierung", u"Default", None))
        self.comboBox.setItemText(1, QCoreApplication.translate("Visualisierung", u"CShuttle::SendOrder ", None))
        self.comboBox.setItemText(2, QCoreApplication.translate("Visualisierung", u"CShuttle::Status ", None))

        self.menuStatus.setTitle(QCoreApplication.translate("Visualisierung", u"File", None))
    # retranslateUi

    def filter_listView(self):

        filteredlistview = []

        for line in self.lines:
            if self.regex_filter_text in line:
                filteredlistview.append(line)

        if filteredlistview:
            self.listView.model().clear()

            for i in filteredlistview:
                item = QStandardItem(i)
                self.model.appendRow(item)

    def filter_by_box_clicked(self):
        self.regex_filter_text = str(self.comboBox.currentText())
        self.filter_listView()
        print(self.regex_filter_text)

    def handle_Events(self, Visualisierung):

        self.importAction.triggered.connect(self.open_file)
        self.comboBox.currentIndexChanged.connect(self.filter_by_box_clicked)
        self.listView.clicked.connect(self.on_list_clicked)

    def on_list_clicked(self):
        items = self.listView.selectedIndexes()
        for it in items:
            print(it.data())

    @staticmethod
    def create_error_msg():
        error_msg = QMessageBox()
        error_msg.setWindowTitle("Fehler beim Laden der Datei")
        error_msg.setText("Datei konnte nicht geladen werden")
        error_msg.setIcon(QMessageBox.Critical)
        error_msg.exec_()

    def open_file(self):
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.ExistingFiles)
        dialog.setFilter(QDir.Files)
        allowed_files = ('.txt', '.log', '.trace')
        dialog.setNameFilter("Logdateien (*.txt *.log *.trace)")

        if dialog.exec_():                                                              ##
            file_name = dialog.selectedFiles()
            if file_name[0].endswith(allowed_files):

                self.model = QStandardItemModel()
                self.listView.setModel(self.model)
                self.listView.setEditTriggers(QAbstractItemView.NoEditTriggers)         ##

                with open(file_name[0], 'r') as f:
                    self.file_data = f.read()
                    self.lines = self.file_data.splitlines()

                    for i in self.lines:
                        item = QStandardItem(i)
                        self.model.appendRow(item)

                self.comboBox.model().item(1).setEnabled(True)
                self.comboBox.model().item(2).setEnabled(True)

            else:
                self.create_error_msg()


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_Visualisierung()
        self.ui.setupUi(self)
        self.ui.handle_Events(self)

    def customChanges(self):
        self.ui.comboBox.model().item(0).setEnabled(False)


if __name__ == "__main__":
    app = QApplication(sys.argv)                                                        ##

    log_line1 = "23.01 11:39:09:605 @06 !0! CShuttle::Status - TW3 FKT=DL0000 SL0300 DR0000 SR0000 TT0000 STAT=7F7E0029 TE=0000 SE=0001 0000 0000 0000 0000 L1=166 L2=0 L3=0 L4=0 AP=8060"
    log_line2 = "23.01 11:39:09:387 @06 !0! CShuttle::Status - TW1 FKT=DL0000 SL0000 DR0000 SR0000 TT0000 STAT=7F7F0025 TE=0000 SE=0001 0000 0000 0000 0000 L1=0 L2=160 L3=0 L4=0 AP=2240"
    log_line_mist = "lakgjlaekjglakgjlaejgaekgljaegk"
    obj_line1 = parse_status_log_line(log_line1)
    obj_line2 = parse_status_log_line(log_line2)
    print('Parsed Zeile 1', obj_line1)
    print('Parsed Zeile 2', obj_line2)
    print('Parsed Zeile Mist', parse_status_log_line(log_line_mist))
    print('Zeile 1 war automatisch?', obj_line2['stat']['bAuto'])

    window = MainWindow()
    window.show()

    sys.exit(app.exec_())




 